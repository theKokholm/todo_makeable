<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'TaskController@index')->name('home');

Route::post('/task/store', 'TaskController@store');

Route::patch('/task/complete/{task}', 'TaskController@complete');

Route::patch('/task/update/{task}', 'TaskController@update');

Route::delete('/task/delete/{task}', 'TaskController@destroy');