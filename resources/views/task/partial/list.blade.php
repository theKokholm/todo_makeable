<table class="table table-striped">
        <tr>
            <th>Task</th>
            <th>Done</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        @foreach($tasks as $task)
            <tr id="taskBodyFor_{{ $task->id }}">
                <td>
                    <span data-id="{{ $task->id }}">{{ $task->body }}</span>
                    <div class="row" style="display: none">
                        <div class="col-xs-2">
                            <button class="btn btn-block btn-primary patchTaskBtn" data-id="{{ $task->id }}">Edit</button>
                        </div>
                        <div class="col-xs-10">
                            <input type="text" name="body_patch" class="form-control" data-id="{{ $task->id }}" value="{{ $task->body }}">
                        </div>

                    </div>
                </td>
                <td>
                    <span class="fa-stack fa-lg completeTaskBtn" data-id="{{ $task->id }}">
                          <i class="fa fa-circle-thin fa-stack-2x text-success"></i>
                          <i class="fa fa-check fa-stack-1x text-success"></i>
                    </span>
                </td>
                <td>
                    <span class="fa-stack fa-lg editTaskBtn" data-id="{{ $task->id }}">
                          <i class="fa fa-circle-thin fa-stack-2x text-info"></i>
                          <i class="fa fa-pencil fa-stack-1x text-info"></i>
                    </span>
                </td>
                <td style="text-align: center">
                     <span class="fa-stack fa-lg deleteTaskBtn" data-id="{{ $task->id }}">
                          <i class="fa fa-circle-thin fa-stack-2x text-danger"></i>
                          <i class="fa fa-trash fa-stack-1x text-danger"></i>
                    </span>
                </td>
            </tr>
        @endforeach
</table>

