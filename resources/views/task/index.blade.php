@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">

                    <div class="col-xs-12">
                        <h2>ToDo for {{ auth()->user()->name }}</h2>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="panel-body">
                                <form method="post" action="task/store/?desc={{ $request->desc }}">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label for="body">Opgave</label>
                                                <input type="text" name="body" id="body" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-md-3">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-success btn-block">Opret</button>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 col-sm-offset-4 col-md-3 col-md-offset-6">
                                            @if(app('request')->input('desc'))
                                                <a href="?desc=0" class="btn btn-primary btn-block">Sorter <span>ASC</span></a>
                                            @else
                                                <a href="?desc=1" class="btn btn-primary btn-block">Sorter <span>DESC</span></a>
                                            @endif
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    @if (count($tasks) != 0)
                        @include('task.partial.list')
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection