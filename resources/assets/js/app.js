
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

let completeTaskBtns = document.getElementsByClassName('completeTaskBtn');

if (completeTaskBtns){
    for (let i = 0; i < completeTaskBtns.length; i++){
        completeTaskBtns[i].addEventListener('click', function(){
            completeTask(this.dataset.id);
        });
    }
}

let editTaskBtns = document.getElementsByClassName('editTaskBtn');

if (editTaskBtns){
    for (let i = 0; i < editTaskBtns.length; i++){
        editTaskBtns[i].addEventListener('click', function(){
            showTaskInput(this.dataset.id);
        });
    }
}

let patchTaskBtn = document.getElementsByClassName('patchTaskBtn');

if (patchTaskBtn){
    for (let i = 0; i < patchTaskBtn.length; i++){
        patchTaskBtn[i].addEventListener('click', function(){
            patchTask(this.dataset.id);
        });
    }
}

let deleteTaskBtns = document.getElementsByClassName('deleteTaskBtn');

if (deleteTaskBtns){
    for (let i = 0; i < deleteTaskBtns.length; i++){
        deleteTaskBtns[i].addEventListener('click', function(){
            deleteTask(this.dataset.id);
        });
    }
}

function completeTask(id) {
    $.ajax({
        url: '/task/complete/'+id,
        type: 'PATCH',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: () => {
            hideTask(id);
        }
    });
}

function deleteTask(id) {
    $.ajax({
        url: '/task/delete/'+id,
        type: 'DELETE',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: () => {
            hideTask(id);
        }
    });
}

function patchTask(id){
    let taskBody = document.getElementById('taskBodyFor_'+id);

    let input = taskBody.getElementsByTagName('input')[0];
    let span = taskBody.getElementsByTagName('span')[0];

    $.ajax({
        url: '/task/update/'+id,
        type: 'PATCH',
        data: {
            'body': input.value
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: () => {
            updateTaskVisual(span, input);
            hideTaskInput(id);
        }
    });
}

function updateTaskVisual(element, input){
    element.innerHTML = input.value;
}

function showTaskInput(id){
    let taskBody = document.getElementById('taskBodyFor_'+id);

    let span = taskBody.getElementsByTagName('span')[0];
    let row = taskBody.getElementsByClassName('row')[0];

    span.style.display = 'none';
    row.style.display = 'block';

    row.getElementsByTagName('input')[0].focus();
}

function hideTaskInput(id){
    let taskBody = document.getElementById('taskBodyFor_'+id);

    let span = taskBody.getElementsByTagName('span')[0];
    let row = taskBody.getElementsByClassName('row')[0];

    span.style.display = 'block';
    row.style.display = 'none';
}

function hideTask(id) {
    let taskBody = document.getElementById('taskBodyFor_'+id);

    taskBody.style.display = 'none';
}