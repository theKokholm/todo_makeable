<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\User::all();

        foreach ($users as $user){
            factory(App\Task::class, 10)
                ->create(['user_id' => $user->id]);
        }
    }
}
