<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = auth()->user();

        if (!$user){
            return redirect('login');
        }

        $tasks = $user->tasks();

        if ($request->desc){
            $tasks = $tasks->descending();
        }

        $tasks = $tasks->incomplete()->get();


        return view('task.index')->with(compact('tasks', 'request'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        auth()->user()->tasks()->create($request->all());

        if ($request->desc){
            return redirect('/?desc=1');
        }

        return redirect()->home();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        $task->body = $request->body;

        $task->save();
    }

    public function complete(Task $task){
        $task->completed = 1;

        $task->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        $task->delete();
    }
}
