<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'body'
    ];

    public function user(){
        $this->belongsTo('App\User');
    }

    public function scopeIncomplete($query){
        return $query->where('completed', 0);
    }

    public function scopeComplete($query){
        return $query->where('completed', 1);
    }

    public function scopeDescending($query){
        return $query->orderBy('id', 'DESC');
    }
}
